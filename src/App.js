
import './holaMundo.css';
import Saluda from './HolaMundo';
import Bola from './Bola';
import Cuadrado from './Cuadrado';
import Separador from './Separador';
import Titulo from './Titulo';
import Capital from './Capital';
import Gato2 from './Gato2';
//import BolaBingo from './BolaBingo';
import Mosca from './Mosca';
//import Bola2 from './Bola2';
import React from 'react';
//import BolaBingoArray from './BolaBingoArray';
import ListaCompra from './ListaCompra';
import Agenda from './Agenda';
import 'bootstrap/dist/css/bootstrap.min.css';

// function Bolera(props){
//   let bolas =[];
// for (let i=0; i<props.numero; i++){
//   bolas.push(<Bola key={i}/>)
// // key{i} --> ponerlo en un bucle en React.  
// }
// // entre corchetes porque es una variable
//   return <>{bolas}</>
// }


// function Bingo(props){
//   let bolas =[];
// for (let i=0; i<props.numero; i++){
//   bolas.push(<Bola key={i}/>)
// // key{i} --> ponerlo en un bucle en React.  
// }
// // entre corchetes porque es una variable
//   return <>{bolas}</>
// };

// function Bingo(props){
// let sorteo =[];
// let a=0;
// for (let valor of props.lista){
// sorteo.push(<BolaBingoArray key={a++} numero={valor} />)
// }
// };


function App() {
 // let cosas = ['agua','manzana','cerveza'];

  // let listaNumeros = [4,6,4,2,4];
  let cosa = (
    <div>
      <h1>Hola, mundo</h1>
      <Saluda saludo="Hola, " receptor="mundo" />
      <Bola/>
      <Cuadrado/>
      <Separador/>
      <Titulo texto="Hola React!" />
      <Titulo texto="Hola React22!" />
      <Titulo texto="Hola React3!" />
      <Capital nom="Barcelona" />
      <Gato2 ancho="200" alto="200" nombre="Garfield" />
      {/* <BolaBingo num="5" /> */}
      <Mosca color="blue"/>
      {/* <Bolera numero={6}/>  
      <Bingo/>
      <Bingo lista={listaNumeros}/> */}
      <Agenda />
      {/* pasar un argumento */}

      <ListaCompra />
    </div>
  );
  return cosa;
}

export default App;