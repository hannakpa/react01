import Table from 'react-bootstrap/Table';


//  contacto es uno de los elementos del array
function convierte_a_Tabla(contacto,i){
    return (
        <tr key={i}>
            <td>{contacto.nombre}</td>
            <td>{contacto.apellido}</td>
            <td>{contacto.email}</td>
        </tr>
    )
}

function Agenda()  {

const misContactos = [
    {nombre:"pep", apellido:"garcia", email:"alks@sldsdk"},
    {nombre:"juan", apellido:"garcia", email:"alks@sldsdk"},
    {nombre:"jose", apellido:"garcia", email:"alks@sldsdk"}
]

    let filas_de_la_tabla= misContactos.map(convierte_a_Tabla);
    return (
        <Table striped bordered hover variant="dark">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Email</th>
                </tr>
            </thead>
            <tbody>
                {filas_de_la_tabla}
            </tbody>
        </Table>
    )
}

export default Agenda