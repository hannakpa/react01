import "./gato.css";

function Gato (propiedades){
    let url="http://placekitten.com/"+propiedades.ancho+"/"+propiedades.alto;

    return (
    <div className="caja-gato">
        <img src={url} alt="gato"/>
        <hr/>
    </div>
    )
};

export default Gato;