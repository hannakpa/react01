import './bolaBingo.css';
import './bola.css';

function BolaBingo (props){
    let numBola = props.num;


    return (
            <div className="bola">
                <p className="num-bola">{numBola}</p>
            </div>
        
    )
};

export default BolaBingo;