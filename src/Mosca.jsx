import { DiAndroid } from "react-icons/di";
import "./mosca.css";

function Mosca(props){
 let color=props.color;
    return(
        <div>
            <DiAndroid color={color}/>
         </div>
    );

}
export default Mosca;