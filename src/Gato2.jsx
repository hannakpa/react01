import './gato2.css';

function Gato2(propiedades){
    let url='http://placekitten.com/'+propiedades.ancho+'/'+propiedades.alto;
    let nombre=propiedades.nombre;

    return ( <div className="caja-gato">
    <img src={url} alt={nombre}/>
    <p className="nombre">{nombre}</p>
</div>)

}


export default Gato2