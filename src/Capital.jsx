import "./capital.css";

function Capital(props){
  let inicial = props.nom[0];
  let resto= props.nom.substring(1);

    return (
    <p className="restoCapital">
        <span className="inicialCapital">{inicial}</span>{resto}
    </p>
    )
    
}

export default Capital;
