function ListaCompra(props){

    let items=props.lista.map((e,indice) =><li key={indice}>{e}</li>);
    // hay dos variables. E, que es el elemento que entra e INDICE del array de key

    return(

        <ul>
            {items}
        </ul>
    )


}

export default ListaCompra;